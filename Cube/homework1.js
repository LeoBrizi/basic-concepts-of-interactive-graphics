"use strict";

var canvas;
var gl;

var numVertices  = 36;

var texSize = 512;
var numChecks = 32;
var c;

var program;

var pointsArray = [];
//var colorsArray = [];
var normalsArray = [];
var texCoordsArray = [];

//var texture;
var texture1, texture2;

var texCoord = [
    vec2(0, 0),
    vec2(0, 1),
    vec2(1, 1),
    vec2(1, 0)
];

var textureOnOff = true, textureOnOffLoc; 
//point one
//position of the viewer
var radius = 2.0;
var theta  = 0.0;
var phi = 0.0;
//viewer Volume for the perspective
var volNear = 0.1;
var volFar = 4.0;
var volLeft = -1.0;
var volRight = 1.0;
var volTop = 1.0;
var volBottom = -1.0;

//point two
//traslation and scaling
var scale = 1.0;
var traslationX = 0.0;
var traslationY = 0.0;
var traslationZ = 0.0;

var aspect;
var canvasW;
var canvasH;
var fieldOfView = 90.0;

//matrices for viewer and projection (point one)
var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;

//matrices for scaling and traslation (point two)
var scalingTraslationMatrix;// traslationMatrix;
var scalingTraslationMatrixLoc;// traslationMatrixLoc;

var eye;
var at = vec3(0.0, 0.0, 0.0);
var up = vec3(0.0, 1.0, 0.0);

var lightPosition = vec4(1.0, 1.0, 1.0, 1.0 );
var lightX = 1.0;
var lightY = 1.0;
var lightZ = 1.0;
var lightAmbient = vec4(0.2, 0.2, 0.2, 1.0 ); //Ambient light is the light that permeates the scene
var lightDiffuse = vec4( 2.0, 2.0, 2.0, 1.0 ); 
var lightSpecular = vec4( 1.0, 1.0, 1.0, 1.0 );

//ruby
var materialAmbient = vec4(0.1745, 0.01175, 0.01175);
var materialDiffuse = vec4(0.61424, 0.04136, 0.04136);
var materialSpecular = vec4(0.727811, 0.626959, 0.626959);
var materialShininess = 76.8;

//var materialAmbient = vec4( 1.0, 0.0, 1.0, 1.0 );
//var materialDiffuse = vec4( 1.0, 0.8, 0.0, 1.0);
//var materialSpecular = vec4( 1.0, 0.8, 0.0, 1.0 );
//var materialShininess = 20.0;


var ambientProductLoc, diffuseProductLoc, specularProductLoc, lightPositionLoc, shininessLoc;
var ambientColor, diffuseColor, specularColor;

// For select type of shading
var gouraudPhong = true;
var fragmentAmbientProductLoc, fragmentDiffuseProductLoc, fragmentSpecularProductLoc, fragmentShininessLoc;
var gouraudPhongLoc;

var vertices = [
    vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4( 0.5,  0.5,  0.5, 1.0 ),
    vec4( 0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4( 0.5,  0.5, -0.5, 1.0 ),
    vec4( 0.5, -0.5, -0.5, 1.0 )
];

var vertexColors = [
    vec4( 0.0, 0.0, 0.0, 1.0 ),  // black
    vec4( 1.0, 0.0, 0.0, 1.0 ),  // red
    vec4( 1.0, 1.0, 0.0, 1.0 ),  // yellow
    vec4( 0.0, 1.0, 0.0, 1.0 ),  // green
    vec4( 0.0, 0.0, 1.0, 1.0 ),  // blue
    vec4( 1.0, 0.0, 1.0, 1.0 ),  // magenta
    vec4( 0.0, 1.0, 1.0, 1.0 ),  // white
    vec4( 0.0, 1.0, 1.0, 1.0 )   // cyan
];

//function configureTexture( image ) {
//    texture = gl.createTexture();
//    gl.bindTexture( gl.TEXTURE_2D, texture );
//    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
//    gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGB,
//         gl.RGB, gl.UNSIGNED_BYTE, image );
//    gl.generateMipmap( gl.TEXTURE_2D );
//    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
//                      gl.NEAREST_MIPMAP_LINEAR );
//    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
//
//    gl.uniform1i(gl.getUniformLocation(program, "texture"), 0);
//}

var image1 = new Uint8Array(4*texSize*texSize);

    for ( var i = 0; i < texSize; i++ ) {
        for ( var j = 0; j <texSize; j++ ) {
            var patchx = Math.floor(i/(texSize/numChecks));
            if(patchx%2) c = 255;
            else c = 0;
            image1[4*i*texSize+4*j] = c;
            image1[4*i*texSize+4*j+1] = c;
            image1[4*i*texSize+4*j+2] = c;
            image1[4*i*texSize+4*j+3] = 255;
        }
}

var image2 = new Uint8Array(4*texSize*texSize);

    // Create a checkerboard pattern
    for ( var i = 0; i < texSize; i++ ) {
        for ( var j = 0; j <texSize; j++ ) {
            var patchy = Math.floor(j/(texSize/numChecks));
            if(patchy%2) c = 255;
            else c = 0;
            image2[4*i*texSize+4*j] = c;
            image2[4*i*texSize+4*j+1] = c;
            image2[4*i*texSize+4*j+2] = c;
            image2[4*i*texSize+4*j+3] = 255;
           }
}

function configureTexture() {
    texture1 = gl.createTexture();
    gl.bindTexture( gl.TEXTURE_2D, texture1 );
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, texSize, texSize, 0, gl.RGBA, gl.UNSIGNED_BYTE, image1);
    gl.generateMipmap( gl.TEXTURE_2D );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
                      gl.NEAREST_MIPMAP_LINEAR );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

    texture2 = gl.createTexture();
    gl.bindTexture( gl.TEXTURE_2D, texture2 );
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, texSize, texSize, 0, gl.RGBA, gl.UNSIGNED_BYTE, image2);
    gl.generateMipmap( gl.TEXTURE_2D );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
                      gl.NEAREST_MIPMAP_LINEAR );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
}

function quad(a, b, c, d) {
    var t1 = subtract(vertices[b], vertices[a]);
    var t2 = subtract(vertices[c], vertices[b]);
    var normal = cross(t1, t2);
    var normal = vec3(normal);

    pointsArray.push(vertices[a]);
    normalsArray.push(normal);
    //colorsArray.push(vertexColors[a]);
    texCoordsArray.push(texCoord[0]);

    pointsArray.push(vertices[b]);
    normalsArray.push(normal);
    //colorsArray.push(vertexColors[a]);
    texCoordsArray.push(texCoord[1]);

    pointsArray.push(vertices[c]);
    normalsArray.push(normal);
    //colorsArray.push(vertexColors[a]);
    texCoordsArray.push(texCoord[2]);

    pointsArray.push(vertices[a]);
    normalsArray.push(normal);
    //colorsArray.push(vertexColors[a]);
    texCoordsArray.push(texCoord[0]);

    pointsArray.push(vertices[c]);
    normalsArray.push(normal);
    //colorsArray.push(vertexColors[a]);
    texCoordsArray.push(texCoord[2]);

    pointsArray.push(vertices[d]);
    normalsArray.push(normal);
    //colorsArray.push(vertexColors[a]);
    texCoordsArray.push(texCoord[3]);
}

function colorCube()
{
    quad( 1, 0, 3, 2 );
    quad( 2, 3, 7, 6 );
    quad( 3, 0, 4, 7 );
    quad( 6, 5, 1, 2 );
    quad( 4, 5, 6, 7 );
    quad( 5, 4, 0, 1 );
}


window.onload = function init() {

    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    canvasW = gl.canvas.width;
    canvasH = gl.canvas.height;
    aspect = canvasW/canvasH;
    
    gl.viewport( 0, 0, canvasW, canvasH );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    gl.enable(gl.DEPTH_TEST);

    //
    //  Load shaders and initialize attribute buffers
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    colorCube();
    
    // Vector for normals
    var nBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normalsArray), gl.STATIC_DRAW );

    var vNormal = gl.getAttribLocation( program, "vNormal" );
    gl.vertexAttribPointer( vNormal, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vNormal );

    //var cBuffer = gl.createBuffer();
    //gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    //gl.bufferData( gl.ARRAY_BUFFER, flatten(colorsArray), gl.STATIC_DRAW );

    //var vColor = gl.getAttribLocation( program, "vColor" );
    //gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    //gl.enableVertexAttribArray( vColor );

    //Vector for vertex
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    //for texture
    var tBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, tBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(texCoordsArray), gl.STATIC_DRAW );

    var vTexCoord = gl.getAttribLocation( program, "vTexCoord" );
    gl.vertexAttribPointer( vTexCoord, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vTexCoord );

    //var image = document.getElementById("texImage");
    //configureTexture( image );

    configureTexture();

    gl.activeTexture( gl.TEXTURE0 );
    gl.bindTexture( gl.TEXTURE_2D, texture1 );
    gl.uniform1i(gl.getUniformLocation( program, "Tex0"), 0);

    gl.activeTexture( gl.TEXTURE1 );
    gl.bindTexture( gl.TEXTURE_2D, texture2 );
    gl.uniform1i(gl.getUniformLocation( program, "Tex1"), 1);


    var ambientProduct = mult(lightAmbient, materialAmbient);
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);
    var specularProduct = mult(lightSpecular, materialSpecular);

    //send info to GPU
    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
    scalingTraslationMatrixLoc = gl.getUniformLocation( program, "scalingTraslationMatrix" );
    //traslationMatrixLoc = gl.getUniformLocation( program, "traslationMatrix" );
    ambientProductLoc = gl.getUniformLocation(program, "ambientProduct");
    diffuseProductLoc = gl.getUniformLocation(program, "diffuseProduct");
    specularProductLoc = gl.getUniformLocation(program, "specularProduct");
    lightPositionLoc = gl.getUniformLocation(program, "lightPosition");
    shininessLoc = gl.getUniformLocation(program, "shininess");

    fragmentAmbientProductLoc = gl.getUniformLocation(program, "fragmentAmbientProduct");
    fragmentDiffuseProductLoc = gl.getUniformLocation(program, "fragmentDiffuseProduct");
    fragmentSpecularProductLoc = gl.getUniformLocation(program, "fragmentSpecularProduct");
    fragmentShininessLoc = gl.getUniformLocation(program, "fragmentShininess");
    gouraudPhongLoc = gl.getUniformLocation(program, "gouraudPhong");
    textureOnOffLoc = gl.getUniformLocation(program, "textureOnOff");

    //don't change
    gl.uniform4fv(ambientProductLoc, flatten(ambientProduct));
    gl.uniform4fv(diffuseProductLoc, flatten(diffuseProduct) );
    gl.uniform4fv(specularProductLoc, flatten(specularProduct) );
    gl.uniform4fv(lightPositionLoc, flatten(lightPosition) );
    gl.uniform1f(shininessLoc,materialShininess);

    gl.uniform4fv( fragmentAmbientProductLoc, flatten(ambientProduct) );
    gl.uniform4fv( fragmentDiffuseProductLoc, flatten(diffuseProduct) );
    gl.uniform4fv( fragmentSpecularProductLoc, flatten(specularProduct) );
    gl.uniform1f( fragmentShininessLoc, materialShininess );
    

    //callbacks for slider and button
    document.getElementById("radiusSlider").onchange = function(event) {
       radius = event.target.value;
    };
    document.getElementById("thetaSlider").onchange = function(event) {
        theta = event.target.value* Math.PI/180.0;
    };
    document.getElementById("phiSlider").onchange = function(event) {
        phi = event.target.value* Math.PI/180.0;
    };
    document.getElementById("nearSlider").onchange = function(event) {
        volNear = this.valueAsNumber;
    };
    document.getElementById("farSlider").onchange = function(event) {
        volFar = this.valueAsNumber;
    };
    document.getElementById("resetViewerPosition").onclick = function() {
        document.getElementById('radiusSlider').value = 2.0;
        document.getElementById('thetaSlider').value = 0.0;
        document.getElementById('phiSlider').value = 0.0;
        radius = 2.0;
        theta  = 0.0;
        phi = 0.0;
    };
    document.getElementById("resetViewerVolume").onclick = function() {
        document.getElementById('nearSlider').value = 0.1;
        document.getElementById('farSlider').value = 4.0;
        volNear = 0.1;
        volFar = 4.0;
    };

    document.getElementById("scalingSlider").onchange = function(event) {
        scale = this.valueAsNumber;
    };
    document.getElementById("traslationXSlider").onchange = function(event) {
        traslationX = this.valueAsNumber;
    };
    document.getElementById("traslationYSlider").onchange = function(event) {
        traslationY = this.valueAsNumber;
    };
    document.getElementById("traslationZSlider").onchange = function(event) {
        traslationZ = this.valueAsNumber;
    };

    document.getElementById("resetScalingTraslation").onclick = function() {
        document.getElementById('scalingSlider').value = 1.0;
        document.getElementById('traslationXSlider').value = 0.0;
        document.getElementById('traslationYSlider').value = 0.0;
        document.getElementById('traslationZSlider').value = 0.0;
        scale = 1.0;
        traslationX = 0.0;
        traslationY = 0.0;
        traslationZ = 0.0;
    };

    document.getElementById("fieldOfViewSlider").onchange = function(event) {
        fieldOfView = this.valueAsNumber;
    };
    document.getElementById("resetFieldOfView").onclick = function() {
        document.getElementById('fieldOfViewSlider').value = 90;
        fieldOfView = 90;
    };

    document.getElementById("lightXSlider").onchange = function(event) {
        lightX = this.valueAsNumber;
    };
    document.getElementById("lightYSlider").onchange = function(event) {
        lightY = this.valueAsNumber;
    };
    document.getElementById("lightZSlider").onchange = function(event) {
        lightZ = this.valueAsNumber;
    };

    document.getElementById("resetLightPosition").onclick = function() {
        document.getElementById('lightXSlider').value = 1.0;
        document.getElementById('lightYSlider').value = 1.0;
        document.getElementById('lightZSlider').value = 1.0;
        lightX = 1.0;
        lightY = 1.0;
        lightZ = 1.0;
    };

    document.getElementById("changeShading").onclick = function() {
        gouraudPhong = !gouraudPhong;
        var elem = document.getElementById("changeShading");
        if(gouraudPhong){
            elem.innerHTML = "Gouraud";
            elem.innerText = "Gouraud";
        }else{
            elem.innerHTML = "Phong";
            elem.innerText = "Phong";
        }
    };

    document.getElementById("deactiveTexture").onclick = function() {
        textureOnOff = !textureOnOff;
        var elem = document.getElementById("deactiveTexture");
        if(textureOnOff){
            elem.innerHTML = "textureOn";
            elem.innerText = "textureOn";
        }else{
            elem.innerHTML = "textureOff";
            elem.innerText = "textureOff";
        }
    };

    render();
}

var render = function() {

    //Matrix for Traslation
    //traslationMatrix = [1.0, 0.0, 0.0, 0.0,
    //                    0.0, 1.0, 0.0, 0.0,
    //                    0.0, 0.0, 1.0, 0.0,
    //                    traslationX, traslationY, traslationZ, 1.0];

    //Matrix for scaling and traslation
    scalingTraslationMatrix = [scale, 0.0, 0.0, 0.0,
                                0.0, scale, 0.0, 0.0,
                                0.0, 0.0, scale, 0.0,
                                traslationX, traslationY, traslationZ, 1.0];

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    eye = vec3(radius*Math.sin(phi), radius*Math.sin(theta), radius*Math.cos(phi));

    lightPosition = vec4(lightX, lightY, lightZ, 1.0 );
    
    modelViewMatrix = lookAt(eye, at , up);
    projectionMatrix = ortho(volLeft, volRight, volBottom, volTop, volNear, volFar);
    //send matrices to gpu
    gl.uniformMatrix4fv( modelViewMatrixLoc, false, flatten(modelViewMatrix) );
    gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );
    gl.uniformMatrix4fv( scalingTraslationMatrixLoc, false, flatten(scalingTraslationMatrix) );
    //gl.uniformMatrix4fv( scalingMatrixLoc, false, flatten(scalingMatrix) );
    gl.uniform4fv(lightPositionLoc, flatten(lightPosition) );
    gl.uniform1i(gouraudPhongLoc, gouraudPhong);
    gl.uniform1i(textureOnOffLoc, textureOnOff);

    gl.enable(gl.SCISSOR_TEST);

    gl.scissor(0, canvasH/2, canvasW / 2, canvasH/2);
    gl.viewport(0, canvasH/2, canvasW / 2, canvasH/2); 
    //orthographic projection
    gl.drawArrays( gl.TRIANGLES, 0, numVertices );

    //perspective projection
    projectionMatrix = perspective(fieldOfView, aspect, volNear, volFar);
    gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );
    
    gl.scissor(canvasW / 2, canvasH/2, canvasW/2, canvasH/2);
    gl.viewport(canvasW / 2, canvasH/2, canvasW/2, canvasH/2);

    gl.drawArrays( gl.TRIANGLES, 0, numVertices );

    requestAnimFrame(render);
}
