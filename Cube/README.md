# hw1
With this first homework I worked on some basic concept of Interactive graphic.
With the sliders of the viewer position you can change the position of the camera. Tha camera moves on a sphere around the cube, with the radius slider you can move the camera away and closer from the cube, with the theta slider you can move the camera up and down and with the phi slider you can move the camera on a circumference around the cube . 
With the sliders of the viewer control you can move the near and far planes of the viewer volume for both projection (orthogonal projection on the left, perspective projection on the right). For the perspective projection there is also a slider to control the field of view.
With the sliders of scaling and traslation you can modify the cube (until now the only thing that moves was the camera). With the scaling slider you can scale by the same quantity all the dimensions of the cube. With the other sliders you can traslate the cube in each direction (X,Y and Z).
With the light position sliders you can move the light, though it is not required I added this possibility to check if the computation for the light was done in the right way. Infact, you can move the light and light up an shadow face of the cube.
The shadind model button make you to switch the shading model between the Gouraud and Phong model.
The last button is used to deactive the texture. I added this possibility to make the difference between the result of the two shading models more visible.
Each set of sliders has a reset button to bring the setting of the set to the origin values.
