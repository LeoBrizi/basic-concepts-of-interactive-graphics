"use strict";

var horse;
var ground;
var obstacle;

var canvas;
var gl;
var program;

var projectionMatrix;
var modelViewMatrix;
var instanceMatrix;

var modelViewMatrixLoc, colorLoc;

var at = vec3(0.0, 0.0, 0.0);
var up = vec3(0.0, 1.0, 0.0);
var eye = vec3(10, 8, 0)

var vertices = [
    vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4( 0.5,  0.5,  0.5, 1.0 ),
    vec4( 0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4( 0.5,  0.5, -0.5, 1.0 ),
    vec4( 0.5, -0.5, -0.5, 1.0 )
];

var textureCoordinate = [
    vec2(0, 0),
    vec2(0, 1),
    vec2(1, 1),
    vec2(1, 0)
];

var numVertices = 24;
var stack = [];

var vertexBuffer;
var pointsArray = [];
var textureCoordinateArray = [];
var textureOnOff = false, textureOnOffLoc; 

var startTIME = 0.0;
var currTIME = startTIME;
var finishTIME = 1.0;
var frameIndex = 0;
var frameNumber = 22;
var stepTime = 0.05;
var doAnimation = false;

function interpolate(startPose, endPose){
    var nextPose = [];
    for(var i = 0; i < 13; ++i){
        nextPose[i] = startPose[i] + ((currTIME - startTIME)/(finishTIME - startTIME))*(endPose[i]-startPose[i]);
    }
    return nextPose;
}

function play(object){
    if(currTIME > finishTIME){
        currTIME = 0.0;
        frameIndex = (frameIndex+1)%frameNumber;
    }
    if(frameIndex == frameNumber-1){
        object.changePose(object.animation[0]);
        frameIndex = 0;
        currTIME = startTIME;
    }
    else{
        var nextPose = interpolate(object.animation[frameIndex], object.animation[(frameIndex+1)%frameNumber]);
        currTIME += stepTime;
        object.changePose(nextPose);
    }
}

function scale4(x, y, z) {
    var scalingMatrix = mat4();
    scalingMatrix[0][0] = x;
    scalingMatrix[1][1] = y;
    scalingMatrix[2][2] = z;
    return scalingMatrix;
}

function newNode(id, size, position, orientation, rotationPoint, texture, color, sibling, child){
    var node = {
        id: id,                 
        size: size,
        position: position,
        orientation: orientation,
        rotationPoint: rotationPoint,
        texture: texture,
        color: color,
        sibling: sibling,
        child: child
    }
    return node;
}

function transformBlock(rotationPoint, position, orientation) {
    var rotationMatrix = rotate(orientation[0], 1, 0, 0);
    rotationMatrix = mult(rotationMatrix, rotate(orientation[1], 0, 1, 0));
    rotationMatrix = mult(rotationMatrix, rotate(orientation[2], 0, 0, 1));
    var transformationMatrix = translate(0, 0, 0);
    transformationMatrix = mult(transformationMatrix, translate(position[0] - rotationPoint[0], position[1] - rotationPoint[1], position[2] - rotationPoint[2]));
    transformationMatrix = mult(transformationMatrix, rotationMatrix);
    transformationMatrix = mult(transformationMatrix, translate(rotationPoint[0], rotationPoint[1], rotationPoint[2]));
    return transformationMatrix;
}

function drawBlock(size, color, texture) {
    gl.uniform4fv(colorLoc, flatten(vec4(color[0]/255, color[1]/255, color[2]/255, 1.0)));
    var instanceMatrix = mult(modelViewMatrix, scale4(size[0], size[1], size[2]));
    gl.uniformMatrix4fv(modelViewMatrixLoc, false, flatten(instanceMatrix));
    if(texture != null){
        textureOnOff = true;
        gl.uniform1i(textureOnOffLoc, textureOnOff);
        for(var i=0; i<6; i++) {
            if (i == 1) {
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, texture[2]);
                gl.uniform1i(gl.getUniformLocation(program, "texture"), 0);
                gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
            }
            else if (i == 5) {
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, texture[1]);
                gl.uniform1i(gl.getUniformLocation(program, "texture"), 0);
                gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
            }
            else  {
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, texture[0]);
                gl.uniform1i(gl.getUniformLocation(program, "texture"), 0);
                gl.drawArrays(gl.TRIANGLE_FAN, 4*i, 4);
            }
        }
    }
    else{
        textureOnOff = false;
        gl.uniform1i(textureOnOffLoc, textureOnOff);
        for(var i =0; i<6; i++) gl.drawArrays(gl.TRIANGLE_FAN, 4*i, 4);
    }
}

function traverse(object, Id) {
    if(Id == null) 
        return;
    stack.push(modelViewMatrix);
    modelViewMatrix = mult(modelViewMatrix, transformBlock(object[Id].rotationPoint, object[Id].position, object[Id].orientation));

    drawBlock(object[Id].size, object[Id].color, object[Id].texture);
    
    if(object[Id].child != null) 
        traverse(object, object[Id].child);
    modelViewMatrix = stack.pop();
    if(object[Id].sibling != null) 
        traverse(object, object[Id].sibling);
}

function quad(a, b, c, d) {
     pointsArray.push(vertices[a]);
     textureCoordinateArray.push(textureCoordinate[0]);
     pointsArray.push(vertices[b]);
     textureCoordinateArray.push(textureCoordinate[1]);
     pointsArray.push(vertices[c]);
     textureCoordinateArray.push(textureCoordinate[2]);
     pointsArray.push(vertices[d]);
     textureCoordinateArray.push(textureCoordinate[3]);
}

function cube()
{
    quad( 2, 1, 0, 3 );
    quad( 2, 3, 7, 6 );
    quad( 3, 0, 4, 7 );
    quad( 6, 5, 1, 2 );
    quad( 6, 5, 4, 7 );
    quad( 5, 4, 0, 1 );
}

window.onload = function init() {

    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 153/255, 1.0, 1.0, 1.0 );
    gl.enable(gl.DEPTH_TEST);

    var aspect = canvas.width/canvas.height;

    program = initShaders( gl, "vertex-shader", "fragment-shader");

    gl.useProgram( program);

    instanceMatrix = mat4();

    projectionMatrix = ortho(-8.0 * aspect, 8.0 * aspect, -8.0, 8.0, 1.0, 25.0);
    modelViewMatrix = mat4();

    textureOnOffLoc = gl.getUniformLocation(program, "textureOnOff");
    colorLoc = gl.getUniformLocation(program, "vertexColor");

    modelViewMatrixLoc = gl.getUniformLocation(program, "modelViewMatrix");
    gl.uniformMatrix4fv(modelViewMatrixLoc, false, flatten(modelViewMatrix));
    gl.uniformMatrix4fv( gl.getUniformLocation( program, "projectionMatrix"), false, flatten(projectionMatrix));

    cube();
    horse = Horse(newNode, gl).horse;
    ground = Ground(newNode).ground;
    obstacle = Obstacle(newNode).obstacle;

    horse.changePose(horse.animation[0]);

    vertexBuffer = gl.createBuffer();

    gl.bindBuffer( gl.ARRAY_BUFFER, vertexBuffer );
    gl.bufferData(gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW);

    var vertexPosition = gl.getAttribLocation( program, "vertexPosition" );
    gl.vertexAttribPointer( vertexPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vertexPosition );

    var textureBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, textureBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(textureCoordinateArray), gl.STATIC_DRAW );

    var vTextureCoordinate = gl.getAttribLocation( program, "vTextureCoordinate" );
    gl.vertexAttribPointer( vTextureCoordinate, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vTextureCoordinate );

    document.getElementById("toggleAnimation").onclick = function() {
        doAnimation = !doAnimation;
    };

    document.getElementById("speedSlider").onchange = function(event) {
        stepTime = (0.05 * this.valueAsNumber)/100;
    };

    render();
}

var render = function() {
        gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        at = horse.object[horse.firstNode].position;
        modelViewMatrix = lookAt(eye, at, up);
        traverse(horse.object, horse.firstNode);
        traverse(obstacle.object, obstacle.firstNode);
        traverse(ground.object, ground.firstNode);
        if(doAnimation)
            play(horse);
        requestAnimFrame(render);
}
