function Obstacle(newNode){
    var colLeftID = 0;
    var colRightID = 1;
    var line1ID = 2;
    var line2ID = 3;
    var wallID = 4;

    var colLeftSize = [1.0, 4.0, 1.0];
    var colRightSize = [1.0, 4.0, 1.0];
    var line1Size = [8, 0.2, 0.2];
    var line2Size = [8, 0.2, 0.2];
    var wallSize = [8, 1.0, 0.4];

    var colLeftPosition = [-4.0, 1.8, 0.2];
    var colRightPosition = [4.0, 1.5, 0.0];
    var line1Position = [4.0, -0.5, 0];
    var line2Position = [4.0, 0.2, 0];
    var wallPosition = [4.0, -1.5, 0];

    var colLeftOrientation = [0, 0, 0];
    var colRightOrientation = [0, 0, 0];
    var line1Orientation = [0, 0, 0];
    var line2Orientation = [0, 0, 0];
    var wallOrientation = [0, 0, 0];

    var colLeftRotationPoint = [0, 0, 0];
    var colRightRotationPoint = [0, 0, 0];
    var line1RotationPoint = [0, 0, 0];
    var line2RotationPoint = [0, 0, 0];
    var wallRotationPoint = [0, 0, 0];

    var colLeftColor = [105,105,105];
    var colRightColor = [105,105,105];
    var line1Color = [0,0,255];
    var line2Color = [0,0,255];
    var wallColor = [255,255,0];

    var obstacle = {firstNode: colLeftID,
        object:[newNode(colLeftID, colLeftSize, colLeftPosition, colLeftOrientation, colLeftRotationPoint, null, colLeftColor, null, line1ID),
        newNode(colRightID, colRightSize, colRightPosition, colRightOrientation, colRightRotationPoint, null, colRightColor, null, null),
        newNode(line1ID, line1Size, line1Position, line1Orientation, line1RotationPoint, null, line1Color, line2ID, null),
        newNode(line2ID, line2Size, line2Position, line2Orientation, line2RotationPoint, null, line2Color, wallID, null),
        newNode(wallID, wallSize, wallPosition, wallOrientation, wallRotationPoint, null, wallColor, null, colRightID)
        ]
    };
    return {
        obstacle: obstacle
    };
};