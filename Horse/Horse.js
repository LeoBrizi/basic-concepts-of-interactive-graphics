function Horse(newNode, gl){

    var torsoID = 0;
    var neckID  = 1;
    var headUpperID = 2;
    var headLowerID = 3;
    var earLeftID = 4;
    var earRightID = 5;
    var leftUpperFrontLegID = 6;
    var leftLowerFrontLegID = 7;
    var rightUpperFrontLegID = 8;
    var rightLowerFrontLegID = 9;
    var leftUpperBackLegID = 10;
    var leftLowerBackLegID = 11;
    var rightUpperBackLegID = 12;
    var rightLowerBackLegID = 13;
    var tailID = 14;
    var maneID = 15;

    var torsoSize = [3.0, 1.3, 1.3];
    var neckSize  = [1.5, 0.7, 0.8];
    var headUpperSize = [1.0, 1.0, 1.0];
    var headLowerSize = [0.7, 1.0, 0.7];
    var earLeftSize = [0.3, 0.8, 0.2];
    var earRightSize = [0.3, 0.8, 0.2];
    var leftUpperFrontLegSize = [0.5, 1.6, 0.4];
    var leftLowerFrontLegSize = [0.40, 1.0, 0.3];
    var rightUpperFrontLegSize = [0.5, 1.6, 0.4];
    var rightLowerFrontLegSize = [0.40, 1.0, 0.3];
    var leftUpperBackLegSize = [0.5, 1.6, 0.4];
    var leftLowerBackLegSize = [0.40, 1.0, 0.3];
    var rightUpperBackLegSize = [0.5, 1.6, 0.4];
    var rightLowerBackLegSize = [0.40, 1.0, 0.3];
    var tailSize = [0.2, 1.4, 0.2];
    var maneSize = [0.4, 1.8, 0.4];

    var torsoPosition =[0, 2.4, -15];
    var neckPosition  = [1.45, 0.75, 0];
    var headUpperPosition = [1.0, 0, 0];
    var headLowerPosition = [0, -0.7, 0];
    var earLeftPosition = [0.9, 0.4, 0.3];
    var earRightPosition = [0.9, 0.4, -0.3];
    var leftUpperFrontLegPosition = [-1.20, -0.8, 0.3];
    var leftLowerFrontLegPosition = [0, -1.3, 0];
    var rightUpperFrontLegPosition = [-1.20, -0.8, -0.3];
    var rightLowerFrontLegPosition = [0, -1.3, 0];
    var leftUpperBackLegPosition = [1.20, -0.8, 0.3];
    var leftLowerBackLegPosition = [0, -1.3, 0];
    var rightUpperBackLegPosition = [1.20, -0.8, -0.3];
    var rightLowerBackLegPosition = [0, -1.3, 0];
    var tailPosition = [-1.5, -0, 0];
    var manePosition = [-0.4, 0.5, 0];

    var torsoOrientation =[0, -90, 0];
    var neckOrientation  = [0, 0, 60];
    var headUpperOrientation = [0, 0, 0];
    var headLowerOrientation = [0, 0, 0];
    var earLeftOrientation = [0, 0, 90];
    var earRightOrientation = [0, 0, 90];
    var leftUpperFrontLegOrientation = [0, 0, 0];
    var leftLowerFrontLegOrientation = [0, 0, 0];
    var rightUpperFrontLegOrientation = [0, 0, 0];
    var rightLowerFrontLegOrientation = [0, 0, 0];
    var leftUpperBackLegOrientation = [0, 0, 0];
    var leftLowerBackLegOrientation = [0, 0, 0];
    var rightUpperBackLegOrientation = [0, 0, 0];
    var rightLowerBackLegOrientation = [0, 0, 0];
    var tailOrientation = [0, 0, -20];
    var maneOrientation = [0, 0, 90];

    var torsoRotationPoint = [0, 0, 0];
    var neckRotationPoint  = [0, 0, 0];
    var headUpperRotationPoint = [0, 0, 0];
    var headLowerRotationPoint = [0, 0, 0];
    var earLeftRotationPoint = [0, 0, 0];
    var earRightRotationPoint = [0, 0, 0];
    var leftUpperFrontLegRotationPoint = [0, -0.8, 0];
    var leftLowerFrontLegRotationPoint = [0, -0.8, 0];
    var rightUpperFrontLegRotationPoint = [0, -0.8, 0];
    var rightLowerFrontLegRotationPoint = [0, -0.8, 0];
    var leftUpperBackLegRotationPoint = [0, -0.8, 0];
    var leftLowerBackLegRotationPoint = [0, -0.8, 0];
    var rightUpperBackLegRotationPoint = [0, -0.8, 0];
    var rightLowerBackLegRotationPoint = [0, -0.8, 0];
    var tailRotationPoint = [0, -0.4, 0];
    var maneRotationPoint = [0, 0, 0];

    var torsoColor = [139, 69, 19];
    var neckColor  = [128, 0, 0];
    var headUpperColor = [139, 69, 19];
    var headLowerColor = [188, 143, 143];
    var earLeftColor = [188, 143, 143];
    var earRightColor = [188, 143, 143];
    var leftUpperFrontLegColor = [128, 0, 0];
    var leftLowerFrontLegColor = [139, 69, 19];
    var rightUpperFrontLegColor = [128, 0, 0];
    var rightLowerFrontLegColor = [139, 69, 19];
    var leftUpperBackLegColor = [128, 0, 0];
    var leftLowerBackLegColor = [139, 69, 19];
    var rightUpperBackLegColor = [128, 0, 0];
    var rightLowerBackLegColor = [139, 69, 19];
    var tailColor = [0, 0, 0];
    var maneColor = [0, 0, 0];

    var textureDimension = 512;
    var numChecks = 8;

    var lateralChessboard = new Uint8Array(4*textureDimension*textureDimension);
    var backChessboard = new Uint8Array(4*textureDimension*textureDimension);
    var frontChessboard = new Uint8Array(4*textureDimension*textureDimension);

    var textureLateralTorso, textureFrontalTorso, textureBackTorso;

    for ( var i = 0; i < textureDimension; i++ ) {
        for ( var j = 0; j <textureDimension; j++ ) {
            var patchX = Math.floor(i/(textureDimension/numChecks));
            var patchY = Math.floor(j/(textureDimension/numChecks));
            if(patchX%2 ^ patchY%2) {
                backChessboard[4*i*textureDimension+4*j] = 128;
                backChessboard[4*i*textureDimension+4*j+1] = 0;
                backChessboard[4*i*textureDimension+4*j+2] = 0;
                lateralChessboard[4*i*textureDimension+4*j] = 128;
                lateralChessboard[4*i*textureDimension+4*j+1] = 128 - i/4;
                lateralChessboard[4*i*textureDimension+4*j+2] = 128 - i/4;
                frontChessboard[4*i*textureDimension+4*j] = 128;
                frontChessboard[4*i*textureDimension+4*j+1] = 128;
                frontChessboard[4*i*textureDimension+4*j+2] = 128;
            }
            else {
                backChessboard[4*i*textureDimension+4*j] = 139;
                backChessboard[4*i*textureDimension+4*j+1] = 69;
                backChessboard[4*i*textureDimension+4*j+2] = 19;
                lateralChessboard[4*i*textureDimension+4*j] = 139;
                lateralChessboard[4*i*textureDimension+4*j+1] = 69;
                lateralChessboard[4*i*textureDimension+4*j+2] = 19;
                frontChessboard[4*i*textureDimension+4*j] = 139;
                frontChessboard[4*i*textureDimension+4*j+1] = 69;
                frontChessboard[4*i*textureDimension+4*j+2] = 19;
            }
        }
    }

    textureLateralTorso = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, textureLateralTorso);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, textureDimension, textureDimension, 0, gl.RGBA, gl.UNSIGNED_BYTE, lateralChessboard);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);

    textureFrontalTorso = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, textureFrontalTorso);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, textureDimension, textureDimension, 0, gl.RGBA, gl.UNSIGNED_BYTE, frontChessboard);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);

    textureBackTorso = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, textureBackTorso);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, textureDimension, textureDimension, 0, gl.RGBA, gl.UNSIGNED_BYTE, backChessboard );
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);

    var torsoTexture = [textureLateralTorso, textureFrontalTorso, textureBackTorso];

    var horse = {firstNode: torsoID,
        object:[newNode(torsoID, torsoSize, torsoPosition, torsoOrientation, torsoRotationPoint, torsoTexture, torsoColor, null, neckID),
        newNode(neckID, neckSize, neckPosition, neckOrientation, neckRotationPoint, null, neckColor, leftUpperFrontLegID, headUpperID),
        newNode(headUpperID, headUpperSize, headUpperPosition, headUpperOrientation, headUpperRotationPoint, null, headUpperColor, null, headLowerID),
        newNode(headLowerID, headLowerSize, headLowerPosition, headLowerOrientation, headLowerRotationPoint, null, headLowerColor, earLeftID, null),
        newNode(earLeftID, earLeftSize, earLeftPosition, earLeftOrientation, earLeftRotationPoint, null, earLeftColor, earRightID, null),
        newNode(earRightID, earRightSize, earRightPosition, earRightOrientation, earRightRotationPoint, null, earRightColor, maneID, null),
        newNode(leftUpperFrontLegID, leftUpperFrontLegSize, leftUpperFrontLegPosition, leftUpperFrontLegOrientation, leftUpperFrontLegRotationPoint, null, leftUpperFrontLegColor, rightUpperFrontLegID, leftLowerFrontLegID),
        newNode(leftLowerFrontLegID, leftLowerFrontLegSize, leftLowerFrontLegPosition, leftLowerFrontLegOrientation, leftLowerFrontLegRotationPoint, null, leftLowerFrontLegColor, null, null),
        newNode(rightUpperFrontLegID, rightUpperFrontLegSize, rightUpperFrontLegPosition, rightUpperFrontLegOrientation, rightUpperFrontLegRotationPoint, null, rightUpperFrontLegColor, leftUpperBackLegID, rightLowerFrontLegID),
        newNode(rightLowerFrontLegID, rightLowerFrontLegSize, rightLowerFrontLegPosition, rightLowerFrontLegOrientation, rightLowerFrontLegRotationPoint, null, rightLowerFrontLegColor, null, null),
        newNode(leftUpperBackLegID, leftUpperBackLegSize, leftUpperBackLegPosition, leftUpperBackLegOrientation, leftUpperBackLegRotationPoint, null, leftUpperBackLegColor, rightUpperBackLegID, leftLowerBackLegID),
        newNode(leftLowerBackLegID, leftLowerBackLegSize, leftLowerBackLegPosition, leftLowerBackLegOrientation, leftLowerBackLegRotationPoint, null, leftLowerBackLegColor, null, null),
        newNode(rightUpperBackLegID, rightUpperBackLegSize, rightUpperBackLegPosition, rightUpperBackLegOrientation, rightUpperBackLegRotationPoint, null, rightUpperBackLegColor, tailID, rightLowerBackLegID),
        newNode(rightLowerBackLegID, rightLowerBackLegSize, rightLowerBackLegPosition, rightLowerBackLegOrientation, rightLowerBackLegRotationPoint, null, rightLowerBackLegColor, null, null),
        newNode(tailID, tailSize, tailPosition, tailOrientation, tailRotationPoint, null, tailColor, null, null),
        newNode(maneID, maneSize, manePosition, maneOrientation, maneRotationPoint, null, maneColor, null, null)
        ],
        changePose: function(pose){
            this.object[torsoID].position =[0, pose[0], pose[1]];
            this.object[neckID].orientation  = [0, 0, pose[2]];
            this.object[leftUpperFrontLegID].orientation = [0, 0, pose[3]];
            this.object[leftLowerFrontLegID].orientation = [0, 0, pose[4]];
            this.object[rightUpperFrontLegID].orientation = [0, 0, pose[5]];
            this.object[rightLowerFrontLegID].orientation = [0, 0, pose[6]];
            this.object[leftUpperBackLegID].orientation = [0, 0, pose[7]];
            this.object[leftLowerBackLegID].orientation = [0, 0, pose[8]];
            this.object[rightUpperBackLegID].orientation = [0, 0, pose[9]];
            this.object[rightLowerBackLegID].orientation = [0, 0, pose[10]];
            this.object[tailID].orientation = [0, 0, pose[11]];
            this.object[torsoID].orientation = [pose[12], -90, 0];
        },
        animation:[[2.4, -15, 40, -45, 0, -35, 0, 30, 0, -20, 0, -60, 0],
            [2.4, -14, 50, -80, 110, -70, 110, -10, -50, -10, 0, -30, 15],
            [2.7, -13, 40, -40, 90, -30, 80, 60, -80, 50, -80, -60, 0],
            [2.4, -11, 30, -60, 40, -50, 60, 60, -60, 45, -60, -30, -15],
            [2.4, -8, 40, -45, 0, -35, 0, 30, 0, -20, 0, -60, 0],
            [2.5, -6, 50, -80, 110, -70, 110, -10, -50, -10, 0, -30, 15],
            [2.4, -5, 40, -40, 90, -30, 80, 60, -80, 50, -80, -60, 0],
            [2.7, -4, 30, -60, 40, -50, 60, 60, -60, 45, -60, -30, -30],
            [3.5, -3, 40, -40, 0, -40, 0, 60, -110, 50, -100, -30, -30],
            [4.0, -1, 50, -60, 50, -60, 50, 70, -60, 60, -80, -70, 0],
            [3.5, 2, 70, -50, 70, -50, 70, 40, -30, 20, -20, -90, 30],
            [2.7, 4, 90, -20, 110, -20, 110, 10, 0, 0, 0, -120, 30],
            [2.4, 7, 60, -40, 10, -40, 10, 60, -80, 50, -80, -60, 0],
            [2.4, 9, 30, -60, 40, -50, 60, 60, -60, 45, -60, -30, -15],
            [2.4, 11, 40, -45, 0, -35, 0, 30, 0, -20, 0, -60, 0],
            [2.4, 14, 50, -80, 110, -70, 110, -10, -50, -10, 0, -30, 15],
            [2.7, 16, 40, -40, 90, -30, 80, 60, -80, 50, -80, -60, 0],
            [2.4, 17, 30, -60, 40, -50, 60, 60, -60, 45, -60, -30, -15],
            [2.4, 19, 40, -45, 0, -35, 0, 30, 0, -20, 0, -60, 0],
            [2.4, 21, 50, -80, 110, -70, 110, -10, -50, -10, 0, -30, 15],
            [2.7, 22, 40, -40, 90, -30, 80, 60, -80, 50, -80, -60, 0],
            [2.4, 24, 30, -60, 40, -50, 60, 60, -60, 45, -60, -30, -15],
            ]
    };

    return {
        horse: horse
    };
};