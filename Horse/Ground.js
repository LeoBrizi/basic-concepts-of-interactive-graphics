function Ground(newNode){
    var groundID = 0;
    var groundSize = [10.0, 0.2, 35.0];
    var groundPosition = [0, -0.2, 0];
    var groundOrientation = [0, 0, 0];
    var groundRotationPoint = [0, 0, 0];
    var groundColor = [107, 142, 35];

    var ground = {firstNode: groundID,
        object:[newNode(groundID, groundSize, groundPosition, groundOrientation, groundRotationPoint, null, groundColor, null, null)]
    };
    return {
        ground: ground
    };
};